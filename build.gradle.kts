import com.diffplug.gradle.spotless.SpotlessExtension
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    kotlin("jvm") version "1.3.61"

    id("com.diffplug.gradle.spotless") version "3.15.0"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("org.springframework.boot") version "2.2.4.RELEASE"
}

apply(plugin = "kotlin")
apply(plugin = "org.springframework.boot")
apply(plugin = "io.spring.dependency-management")

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict")

repositories {
    mavenCentral()
    jcenter()
}

ext {
    set("junit-jupiter.version", "5.6.0")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.codeborne:selenide:5.10.0")
    implementation("com.sun.xml.bind:jaxb-osgi:2.3.2")
    implementation("org.springframework.boot:spring-boot-configuration-processor")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.+")

    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.vintage:junit-vintage-engine")

    api("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "junit")
        exclude(group = "org.hamcrest")
        exclude(group = "org.mockito")
        exclude(group = "org.seleniumhq.selenium")
    }
}

configure<SpotlessExtension> {
    format("misc") {
        target("*/src/**/*.kt")
        endWithNewline()
        trimTrailingWhitespace()
    }
    kotlin {
        target("*/src/**/*.kt")
        ktlint("0.29.0")
    }
    kotlinGradle {
        target("**/*gradle.kts")
        ktlint("0.29.0")
    }
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            showExceptions = true
            exceptionFormat = FULL
            showStackTraces = true
        }

        if (project.hasProperty("browser")) {
            systemProperty("browser", project.property("browser") ?: "chrome")
        }
        systemProperty("selenide.headless", "false")
        if (project.hasProperty("grid")) {
            systemProperty("browser.remote", "true")
            systemProperty("selenide.remote", "http://${project.property("grid")}:4444/wd/hub")
        }
    }
}

tasks.withType<BootJar> {
    enabled = false
}
tasks.withType<Jar> {
    enabled = false
}
