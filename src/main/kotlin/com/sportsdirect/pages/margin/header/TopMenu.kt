package com.sportsdirect.pages.margin.header

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import org.springframework.stereotype.Component

@Component
class TopMenu {
    private val root: SelenideElement = `$`("#topMenu")
    private val uscCss: String = ".MenuGroupUSC"

    fun selectUsc() {
        root.waitUntil(visible, TIMEOUT_LONG.value)
        root.find(uscCss).hover()
    }
}