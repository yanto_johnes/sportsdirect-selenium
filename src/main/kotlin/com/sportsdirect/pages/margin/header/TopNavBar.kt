package com.sportsdirect.pages.margin.header

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.Selenide.`$$`
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import com.sportsdirect.util.Timeout.TIMEOUT_SHORT
import org.springframework.stereotype.Component

@Component
class TopNavBar {
    private val root: SelenideElement = `$`(".TopNavBar")
    private val searchField: SelenideElement = `$`("#txtSearch")
    private val suggestionDdl: SelenideElement = `$`("#ui-id-1")
    private val suggestions: ElementsCollection = `$$`(".ui-menu-item")
    private val searchButton: SelenideElement = `$`("#cmdSearch")

    fun fillSearchField(searchKey: String) {
        root.waitUntil(visible, TIMEOUT_LONG.value)
        searchField.click()
        searchField.`val`(searchKey)
    }

    fun selectFromSuggested(param: String) {
        suggestionDdl.waitUntil(visible, TIMEOUT_SHORT.value)
        suggestions.find { it.text()!!.equals(param, true) }?.click()
    }

    fun executeSearch() {
        searchButton.shouldBe(visible)
        searchButton.click()
    }
}