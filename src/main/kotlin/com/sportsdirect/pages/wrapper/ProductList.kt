package com.sportsdirect.pages.wrapper

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout
import org.springframework.stereotype.Component

@Component
class ProductList {
    private val root: SelenideElement = `$`("#ProductContainer")
    private val productCss: String = "[li-productid='%s']"

    fun selectById(id: String) {
        root.waitUntil(visible, Timeout.TIMEOUT_LONG.value)
        `$`(String.format(productCss, id)).click()
    }
}