package com.sportsdirect.pages.wrapper

import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import org.springframework.stereotype.Component

@Component
class SearchResultHeader {
    private val heading: SelenideElement = `$`("#catHeader")
    private val foundItemCount: SelenideElement = `$`(".totalProducts")

    fun heading(): SelenideElement {
        return heading;
    }

    fun foundItems(): Int {
        return foundItemCount.text().toInt()
    }
}