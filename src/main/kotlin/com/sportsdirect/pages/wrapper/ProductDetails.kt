package com.sportsdirect.pages.wrapper

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Size
import com.sportsdirect.util.Timeout
import org.springframework.stereotype.Component

@Component
class ProductDetails {
    private val root: SelenideElement = `$`("#mainDetails")
    private val productName: SelenideElement = `$`(".last")
    private val productQuantity: SelenideElement = `$`("#ProductQty")
    private val increaseQuantityButton: SelenideElement = `$`(".s-basket-plus-button")
    private val sizeBar: SelenideElement = `$`("#ulSizes")
    private val sizeSelected: SelenideElement = `$`(".sizeVariantHighlight")
    private val addToBagButton: SelenideElement = `$`("#aAddToBag")
    private val sizeSelector: String = "[data-text='%s']"

    fun productName(): String {
        return productName.text()
    }

    fun selectQuantity(count: Int) {
        root.waitUntil(visible, Timeout.TIMEOUT_LONG.value)
        var clicked = 1
        while (count != clicked) {
            increaseQuantityButton.click()
            clicked++
        }
    }

    fun quantitySelected(): Int {
        return productQuantity.value.toInt()
    }

    fun selectSize(size: Size) {
        sizeBar.shouldBe(visible)
        `$`(String.format(sizeSelector, size.value)).click()
        sizeSelected.shouldBe(visible)
    }

    fun addToBag() {
        addToBagButton.shouldBe(visible)
        addToBagButton.click()
    }
}