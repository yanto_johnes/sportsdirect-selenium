package com.sportsdirect.pages.wrapper

import com.codeborne.selenide.Condition.not
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import org.springframework.stereotype.Component

@Component
class Submenu {
    private val uscSubmenu: SelenideElement = `$`(".USCCenter")
    private val uscMen: SelenideElement = `$`("[alt='Mens on USC']")

    fun goToUscMen() {
        uscSubmenu.waitUntil(visible, TIMEOUT_LONG.value)
        uscMen.click()
        uscSubmenu.waitUntil(not(visible), TIMEOUT_LONG.value)
    }
}