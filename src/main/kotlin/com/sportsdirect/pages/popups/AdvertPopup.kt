package com.sportsdirect.pages.popups

import com.codeborne.selenide.Condition
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import com.sportsdirect.util.Timeout.TIMEOUT_SHORT
import org.openqa.selenium.By
import org.springframework.stereotype.Component

@Component
class AdvertPopup {
    private val root: SelenideElement = `$`("#advertPopup")
    private val closeButton: String = ".close"

    fun close() {
        root.waitUntil(visible, TIMEOUT_LONG.value)
        root.findElement(By.cssSelector(closeButton)).click()
        root.waitUntil(Condition.not(visible), TIMEOUT_SHORT.value)
    }
}