package com.sportsdirect.pages.popups

import com.codeborne.selenide.Condition.not
import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import com.sportsdirect.util.Timeout.TIMEOUT_SHORT
import org.springframework.stereotype.Component

@Component
class CookiePopup {
    private val root: SelenideElement = `$`("#divCookieAcceptance")
    private val closeButton: SelenideElement = `$`("#inputAcceptCookies")

    fun accept() {
        root.waitUntil(visible, TIMEOUT_LONG.value)
        closeButton.click()
        root.waitUntil(not(visible), TIMEOUT_SHORT.value)
    }
}