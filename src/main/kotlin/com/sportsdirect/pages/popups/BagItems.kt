package com.sportsdirect.pages.popups

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Selenide.`$`
import com.codeborne.selenide.SelenideElement
import com.sportsdirect.domain.ProductData
import com.sportsdirect.util.Size
import com.sportsdirect.util.Timeout.TIMEOUT_LONG
import org.springframework.stereotype.Component

@Component
class BagItems {
    private val root: SelenideElement = `$`("#divBagItems")
    private val id: String = "[id^=li%s]"
    private val name: String = ".BaskName"
    private val quantity: String = ".BaskQuant"
    private val size: String = ".BaskSize"

    fun getProductData(id: String): ProductData {
        val productData = ProductData(id)
        val productWrapper: SelenideElement = `$`(String.format(this.id, id))
        root.waitUntil(visible, TIMEOUT_LONG.value)
        productData.name = productWrapper.find(name).text()
        productData.quantity = productWrapper.find(quantity).text().replace("Qty: ", "").toInt()
        val size = productWrapper.find(size).text()
        val collect: Map<String, Size> = Size.values().associateBy { it.value }
        productData.size = collect[size]
        return productData
    }
}