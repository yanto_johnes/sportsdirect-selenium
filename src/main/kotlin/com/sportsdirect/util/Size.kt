package com.sportsdirect.util

enum class Size constructor(val value: String) {
    S("Small"),
    M("Medium"),
    L("Large"),
    XL("X Large"),
    XXL("2X Large")
}