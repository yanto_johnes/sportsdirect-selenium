package com.sportsdirect

import com.codeborne.selenide.Selenide
import com.sportsdirect.pages.popups.AdvertPopup
import com.sportsdirect.pages.popups.CookiePopup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class SessionManager {
    @Autowired
    lateinit var advertPopup: AdvertPopup
    @Autowired
    lateinit var cookiePopup: CookiePopup

    fun setup(url: String) {
        Selenide.clearBrowserCookies()
        Selenide.open(url)
    }

    fun closePopups() {
        advertPopup.close()
        cookiePopup.accept()
    }
}