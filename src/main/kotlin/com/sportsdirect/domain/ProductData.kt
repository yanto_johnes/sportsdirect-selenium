package com.sportsdirect.domain

import com.sportsdirect.util.Size

data class ProductData(val id: String) {
    var name: String? = null
    var size: Size? = null
    var quantity: Int? = null
}