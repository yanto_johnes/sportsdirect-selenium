package com.sportsdirect.selenide

import com.codeborne.selenide.Condition.visible
import com.sportsdirect.Config
import com.sportsdirect.SessionManager
import com.sportsdirect.pages.margin.header.TopNavBar
import com.sportsdirect.pages.wrapper.SearchResultHeader
import com.sportsdirect.selenide.scenarios.search.KeyPhraseScenarios
import com.sportsdirect.selenide.scenarios.search.SuggestionScenarios
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import java.util.stream.Stream

@SpringBootTest(classes = [Config::class])
@DisplayName("Product search")
class ProductSearchTests {
    @Autowired
    lateinit var session: SessionManager
    @Autowired
    lateinit var topNavBar: TopNavBar
    @Autowired
    lateinit var suggestionScenarios: SuggestionScenarios
    @Autowired
    lateinit var searchResultHeader: SearchResultHeader
    @Autowired
    lateinit var keyPhraseScenarios: KeyPhraseScenarios

    @Value("\${url}")
    lateinit var url: String

    @TestFactory
    fun `Search field suggestions`(): Stream<DynamicTest?>? {
        return suggestionScenarios.scenario().map {
            DynamicTest.dynamicTest("Searching for: ${it.keyFull}") {
                session.setup(url)
                session.closePopups()
                topNavBar.fillSearchField(it.searchKey)
                topNavBar.selectFromSuggested(it.keyFull)
                validate()
            }
        }
    }

    @TestFactory
    fun `Search by key phrase`(): Stream<DynamicTest?>? {
        return keyPhraseScenarios.scenario().map {
            DynamicTest.dynamicTest("Searching for: ${it.keyPhrase}") {
                session.setup(url)
                session.closePopups()
                topNavBar.fillSearchField(it.keyPhrase)
                topNavBar.executeSearch()
                validate()
            }
        }
    }

    private fun validate() {
        searchResultHeader.heading().shouldBe(visible)
        assertThat(searchResultHeader.foundItems()).isGreaterThan(0);
    }
}