package com.sportsdirect.selenide

import com.sportsdirect.Config
import com.sportsdirect.SessionManager
import com.sportsdirect.pages.margin.header.TopMenu
import com.sportsdirect.pages.popups.BagItems
import com.sportsdirect.pages.wrapper.ProductDetails
import com.sportsdirect.pages.wrapper.ProductList
import com.sportsdirect.pages.wrapper.Submenu
import com.sportsdirect.selenide.scenarios.cart.ProductProvider.LEVIS_HOODY_WOMAN
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [Config::class])
class CartTests {
    @Autowired
    lateinit var session: SessionManager
    @Autowired
    lateinit var topMenu: TopMenu
    @Autowired
    lateinit var submenu: Submenu
    @Autowired
    lateinit var productList: ProductList
    @Autowired
    lateinit var productDetails: ProductDetails
    @Autowired
    lateinit var bagItems: BagItems

    @Value("\${url}")
    lateinit var url: String

    @BeforeEach
    fun before() {
        session.setup(url)
        session.closePopups()
        topMenu.selectUsc()
        submenu.goToUscMen()
    }

    @Test
    fun `add item to cart`() {
        productList.selectById(LEVIS_HOODY_WOMAN.id)
        assertThat(productDetails.productName().equals(LEVIS_HOODY_WOMAN.naming, true)).isTrue()
        productDetails.selectSize(LEVIS_HOODY_WOMAN.size)
        productDetails.selectQuantity(LEVIS_HOODY_WOMAN.quantity)
        assertEquals(LEVIS_HOODY_WOMAN.quantity, productDetails.quantitySelected())
        productDetails.addToBag()
        val productInBasket = bagItems.getProductData(LEVIS_HOODY_WOMAN.id)
        assertThat(productInBasket.name?.equals(LEVIS_HOODY_WOMAN.naming, true)).isTrue()
        assertThat(productInBasket.size?.equals(LEVIS_HOODY_WOMAN.size)).isTrue()
        assertThat(productInBasket.quantity?.equals(LEVIS_HOODY_WOMAN.quantity)).isTrue()
    }
}