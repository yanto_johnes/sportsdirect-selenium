package com.sportsdirect.selenide.scenarios.cart

import com.sportsdirect.util.Size
import com.sportsdirect.util.Size.S

enum class ProductProvider(
        val id: String,
        val naming: String,
        val size: Size,
        val quantity: Int
){
    LEVIS_HOODY_WOMAN("53273301","Levis Levi Mario Tab Hoody Sn02", S, 1)
}