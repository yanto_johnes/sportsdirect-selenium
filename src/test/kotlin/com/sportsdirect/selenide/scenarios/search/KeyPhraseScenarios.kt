package com.sportsdirect.selenide.scenarios.search

import org.springframework.stereotype.Component
import java.util.stream.Stream

@Component
class KeyPhraseScenarios {
    fun scenario(): Stream<KeyPhraseScenario> {
        return Stream.of(
                KeyPhraseScenario("shirt woman black"),
                KeyPhraseScenario("sock man blue")
        )
    }
}

data class KeyPhraseScenario(
        val keyPhrase: String
)