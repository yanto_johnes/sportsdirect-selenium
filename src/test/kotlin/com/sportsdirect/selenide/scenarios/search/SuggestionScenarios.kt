package com.sportsdirect.selenide.scenarios.search

import org.springframework.stereotype.Component
import java.util.stream.Stream

@Component
class SuggestionScenarios {
    fun scenario(): Stream<SuggestionScenario> {
        return Stream.of(
                SuggestionScenario(searchKey = "nike", keyFull = "nike shoes"),
                SuggestionScenario(searchKey = "skechers kids", keyFull = "skechers kids")
        )
    }
}

data class SuggestionScenario(
        val searchKey: String,
        val keyFull: String
)